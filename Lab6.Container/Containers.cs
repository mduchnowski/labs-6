﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;

namespace Lab6.Container
{
    public class Containers : IContainer
    {
        IDictionary<Type, Type> types;
        IDictionary<Type, object> objects;
        IDictionary<Type, Delegate> providers;
        public Containers()
        {
            types = new Dictionary<Type, Type>();
            objects = new Dictionary<Type, object>();
            providers = new Dictionary<Type, Delegate>();
        }
        public void Register(System.Reflection.Assembly assembly)
        {
            foreach (var item in assembly.GetExportedTypes())
            {
                Register(item);
            }
        }

        public void Register(Type type)
        {
            foreach (var item in type.GetInterfaces())
            {
                if (!types.ContainsKey(item))
                    types.Add(item, type);
                else
                    types[item] = type;
            }
        }

        public void Register<T>(T impl) where T : class
        {
            if (typeof(T).IsInterface)
                if (!objects.ContainsKey(typeof(T)))
                    objects.Add(typeof(T), impl);
                else
                    objects[typeof(T)] = impl;
            else
                foreach (var item in typeof(T).GetInterfaces())
                {
                    if (!objects.ContainsKey(item))
                        objects.Add(item, impl);
                    else
                        objects[item] = impl;
                }
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            if (typeof(T).IsInterface)
                if (!providers.ContainsKey(typeof(T)))
                    providers.Add(typeof(T), provider);
                else
                    providers[typeof(T)] = provider;
            else
                foreach (var item in typeof(T).GetInterfaces())
                {
                    if (!providers.ContainsKey(item))
                        providers.Add(item, provider);
                    else
                        providers[item] = provider;
                }
        }

        public T Resolve<T>() where T : class
        {
            return Resolve(typeof(T)) as T;
        }

        public object Resolve(Type type)
        {
            if (type.IsInterface)
            {
                if (objects.ContainsKey(type))
                {
                    return objects[type];
                }
                else if (providers.ContainsKey(type))
                {
                    return providers[type].DynamicInvoke();
                }
                else if (types.ContainsKey(type))
                {
                    List<object> parameters = new List<object>();
                    int count = 0;
                    foreach (var constructors in types[type].GetConstructors())
                    {
                        var parms = constructors.GetParameters();
                        if (parms.Count() > count)
                        {
                            count = parms.Count((x) => (x.ParameterType.IsInterface == true));
                            parameters.Clear();
                            object resolve = null;
                            foreach (var parameter in parms)
                            {
                                if (parameter.ParameterType.IsInterface)
                                    resolve = Resolve(parameter.ParameterType);
                                if (resolve == null)
                                    throw new UnresolvedDependenciesException();
                                parameters.Add(resolve);
                            }
                        }
                    }
                    var test = Activator.CreateInstance(types[type], parameters.ToArray());
                    return test;
                }
                else
                    return null;
            }
            else
                throw new ArgumentException("Type is not an interface");
        }
    }
}
