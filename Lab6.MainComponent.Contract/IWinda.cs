﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.MainComponent.Contract
{
    public interface IWinda
    {
        string Pietro();
        void CzasPodrozy();
        string Drzwi();
    }
}
