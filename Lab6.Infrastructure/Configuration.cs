﻿using PK.Container;
using System;
using System.Reflection;
using Lab6.Container;
using Lab6.ControlPanel.Implementation;
using Lab6.MainComponent.Implementation;

namespace Lab6.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            var container = new Lab6.Container.Containers();
            container.Register(new Lab6.Display.Implementation.Display());
            container.Register(new Lab6.ControlPanel.Implementation.ControlPanels());
            container.Register(new Lab6.MainComponent.Implementation.Winda());

            return container;
        }
    }
}
