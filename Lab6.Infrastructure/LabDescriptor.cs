﻿using Lab6.ControlPanel.Contract;
using PK.Container;
using System;
using System.Reflection;
using Lab6.MainComponent.Contract;
using Lab6.MainComponent.Implementation;
using Lab6.ControlPanel.Implementation;
using Lab6.Container;

namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new Containers();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(ControlPanels));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IWinda));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Winda));

        #endregion
    }
}
