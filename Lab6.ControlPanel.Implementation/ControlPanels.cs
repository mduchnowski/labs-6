﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using System.Windows;

namespace Lab6.ControlPanel.Implementation
{
    public class ControlPanels:IControlPanel
    {
        Window okno;
        public ControlPanels(IWinda winda)
        {
            this.okno = new Window1(winda);
        }
        public System.Windows.Window Window
        {
            get { return okno; }
        }
        public ControlPanels() { }
    }
}
